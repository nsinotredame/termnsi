# Récursivité


## Définition

**Une fonction récursive est une fonction qui contient un appel à elle même**


## Cas naïf



```python
def infini(n):
    return infini(n+1)

print(infini(0))
```
Quel est le problème de cette fonction?

-> Boucle infinie



## Cas plus élaboré

Pour que notre fonction récursive s'arrête à un moment donné, nous allons utiliser une conditionnelle

```python
def a_peu_pres_infini(n):
    if n > 500:
        return n
    else:
        return a_peu_pres_infini(n+1)

print(a_peu_pres_infini(0))
```

Quand la fonction va-t-elle s'arrêter?

-> Quand elle aura atteint 500

## Equivalence entre boucle while et fonction récursive

Chaque fonction récursive peu s'écrire avec une boucle while et inversement

**On appelera "versions itératives" les versions avec boucle while**

```python
def a_peu_pres_infini(n):
    while n < 500:
        n = n + 1
    return n
```

Quel est donc l'intérêt des fonctions récursives?

Dans certaines situations il est plus simple et plus naturel d'écrire une fonction récursive plutôt qu'une boucle while

**Exemple avec les suites mathématiques :**

![](IMG/fonction.png)


```python
def suite(n):
    if n == 0:
        return 2
    else:
        return -2*suite(n-1)
```

La version récursive s'écrit quasiment sans effort, tandis que la version itérative:

```python
def suite_while(n):
    Un = 2
    i = 0
    while i <= n:
        Un = -2 * Un
    
    return Un
```

Vous remarquerez que dans la version itérative, on construit le résultat à partir du cas de base (u0) pour obtenir le résultat final (un), tandis que dans la version récursive, on cherche d'abord à calculer le résultat final pour ensuite calculer le résultat un-1 etc...

## Cas de base et cas récursif

On appelera **cas de base** le cas de la conditionnelle dans lequel il n'y a pas d'appel à la fonction, et **cas récursif** le cas où il y a un appel à la fonction


```python
def suite(n):

    #Cas de base
    if n == 0:
        return n

    #Cas récursif
    else:
        return -2*suite(n-1)
```

**Attention** : Si il n'y a pas de cas de base dans votre fonction récursive, il y aura **forcément** une boucle infinie
