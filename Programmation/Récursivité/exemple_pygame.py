import pygame

pygame.init()

largeur = 800
hauteur = 600


fenetre = pygame.display.set_mode((largeur,hauteur))

#Trace un trait
pygame.draw.line(fenetre, [255]*3, (100,100), (300, 100))
pygame.display.flip()

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

pygame.quit()